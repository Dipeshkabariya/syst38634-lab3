/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan991550231;

import java.util.Scanner;

public class PasswordValidator {

	public static void main(String[] args) {

	}

	/*
	 * @author Dipesh kabariya This Class validates passwords and will be developed
	 * using TDD. A method to check whether a password is at least length 8
	 * 
	 * @return true if length is greater than or equals to 8 and false otherwise
	 */
	private static int Min_Length = 8;
	private static int Min_Num_Digit = 2;

	public static boolean isValidLength(String pass) {
		if (pass != null) {
			return pass.trim().length() >= Min_Length;

		}
		return false;
	}
	
	
	public static boolean hasValidCaseChars(String password) {
		return password !=null && password.matches( ".*[A-Z]+.*" ) && password.matches( ".*[a-z]+.*" );

	}
	
	

	

	/*
	 * Method hasEnoughDigits
	 * This is used for validation of password to content alteast two digit and no 
	 * space plus password to be not less than Min_length.
	 * */
	public static boolean hasEnoughDigits(String password) {
		int num1 = 1;
		char c;
		char[] n = new char[password.length()];

		for (int i = 0; i < password.length(); i++) {
			 c = password.charAt(i);
			 n[i] = c;

			if (Character.isDigit(c)) {

				num1 = num1 + 1;
			}
		}
		if ((num1 == Min_Num_Digit || num1 > Min_Num_Digit) && (password.trim().length() >= Min_Length ))
		{
			return true;
		} else
			return false;
	}
	
	

}
